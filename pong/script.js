let playArea = document.getElementById("playArea");

let rocket1X,rocket1Y;
let rocket2X,rocket2Y;
let rocketH,rocketW;
let rocketDy;

let ballX,ballY;
let ballWH;
let ballDx, ballDy;

let scoreLeft, scoreRight;

let keysPressed;

let context;

let KeyUp1 = "w" , KeyDown1 = "s";
let KeyUp2 = "ArrowUp" , KeyDown2 = "ArrowDown";

function KeyDownHandler(e){
    if(e.key==KeyUp1 || e.key==KeyDown1 || e.key==KeyUp2 || e.key==KeyDown2){
        keysPressed[e.key]=true;
    }
}

function KeyUpHandler(e){
    if(e.key==KeyUp1 || e.key==KeyDown1 || e.key==KeyUp2 || e.key==KeyDown2){
        keysPressed[e.key]=false;
    }
}

function IntialValues()
{
    context = playArea.getContext("2d");

    scaleFactor = Math.sqrt(Math.pow(playArea.width,2)+Math.pow(playArea.height,2));

    rocketH = Math.floor(scaleFactor/5);
    rocketW = Math.floor(rocketH/7);
    rocket1X = 0;
    rocket1Y = Math.floor(playArea.height/2-rocketH/2);

    rocket2X = playArea.width-rocketW;
    rocket2Y = rocket1Y;

    rocketDy = Math.floor(rocketH/20);

    ballWH = rocketW;
    ballX = Math.floor(playArea.width/2-ballWH/2);
    ballY = Math.floor(playArea.height/2-ballWH/2);

    ballDx = 1;
    ballDy = 1;

    keysPressed = [];

    document.addEventListener("keydown",KeyDownHandler,false);
    document.addEventListener("keyup",KeyUpHandler,false);
}

function RenderObjects()
{
    context.clearRect(0,0,playArea.width,playArea.height);

    context.fillStyle="blue";
    context.fillRect(rocket1X,rocket1Y,rocketW,rocketH);
    context.fillRect(rocket2X,rocket2Y,rocketW,rocketH);
    context.fillRect(ballX,ballY,ballWH,ballWH);
}

function CalculatePhysics()
{
    if(keysPressed[KeyUp1]==true)
    {
        rocket1Y-=rocketDy;
        if(rocket1Y<0)
        {
            rocket1Y=0;
        }
    }    
    if(keysPressed[KeyDown1]==true)
    {
        rocket1Y+=rocketDy;
        if(rocket1Y+rocketH>playArea.height)
        {
            rocket1Y=playArea.height-rocketH;
        }
    }    

    if(keysPressed[KeyUp2]==true)
    {
        rocket2Y-=rocketDy;
        if(rocket2Y<0)
        {
            rocket2Y=0;
        }
    }    
    if(keysPressed[KeyDown2]==true)
    {
        rocket2Y+=rocketDy;
        if(rocket2Y+rocketH>playArea.height)
        {
            rocket2Y=playArea.height-rocketH;
        }
    } 

    ballX+=ballDx;
    ballY+=ballDy;

    if(ballY+ballWH>=playArea.height || ballY<=0)
    {
        ballDy=-ballDy;
    }

    if(ballX+ballWH>=playArea.width || ballX<=0)
    {
        ballDx=-ballDx;
    }
}

function UpdateGame()
{
    CalculatePhysics();
    RenderObjects();
}

IntialValues();

setInterval(UpdateGame,10);