let squareDiv = document.getElementById("squareDiv");

function GetRandomColor(){
    let color = Math.random();
    color = Math.round(color*256);
    return color;
}

function MakeDivRandomColor(){
    let r = GetRandomColor();
    let g = GetRandomColor();
    let b = GetRandomColor();
    let color = "rgb("+r+","+g+","+b+")";
    squareDiv.style.backgroundColor = color;
}

function ClickButton(){
    setInterval(MakeDivRandomColor,100);
}